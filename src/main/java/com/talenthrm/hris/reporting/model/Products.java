package com.talenthrm.hris.reporting.model;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table  (name = "products")
public class Products {
    

    @Id
    @GeneratedValue
    private int id;
    private String amount;
    private String color;
    private String price;
    private String rating;
}
