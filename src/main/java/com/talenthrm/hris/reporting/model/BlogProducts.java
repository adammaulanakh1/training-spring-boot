package com.talenthrm.hris.reporting.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "products")
public class BlogProducts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String amount;
    String color;
    String price;
    String rating;
    public void save(BlogProducts blogProducts) {
    }
}
