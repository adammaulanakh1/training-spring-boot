package com.talenthrm.hris.reporting.service;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.MediaSize.Other;

import com.talenthrm.hris.reporting.dto.RequestProducts;
import com.talenthrm.hris.reporting.model.BlogProducts;
import com.talenthrm.hris.reporting.model.Products;
import com.talenthrm.hris.reporting.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProductService {
    @Autowired
    ProductRepository blogProducts;

    public List<BlogProducts> listBlogs(){
        BlogProducts blog = new BlogProducts();

        blogProducts.save(blog);

        List<BlogProducts> postings = new ArrayList<>();
        postings = blogProducts.findGreterThan5();
        return postings;
    }

    public String saveBlog(RequestProducts req) {
        BlogProducts blog = new BlogProducts();
        blog.setAmount(req.getAmount());
        blog.setColor(req.getColor());
        blog.setPrice(req.getPrice());
        blog.setRating(req.getRating());
        blogProducts.save(blog);
        return "Success";
    }
    public String deleteProduct(Long id){
        blogProducts.deleteById(id);
        return "product succes removed !!" + id;
    }

    public BlogProducts updateProducts(RequestProducts blogproducts2 ) {
        BlogProducts blog = new BlogProducts();
        blog.setAmount(blogproducts2.getAmount());
        blog.setPrice(blogproducts2.getPrice());
        blog.setRating(blogproducts2.getRating());
        return blogProducts.save(blog);
    }



    
    
}
