package com.talenthrm.hris.reporting.service;

import java.util.ArrayList;
import java.util.List;

import com.talenthrm.hris.reporting.dto.RequestPosting;
import com.talenthrm.hris.reporting.model.BlogPosting;
import com.talenthrm.hris.reporting.repository.BlogRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostingService {
    @Autowired
    BlogRepository blogRepo;
    
    public List<BlogPosting> listBlogs() {
        BlogPosting blog = new BlogPosting();
          
         blogRepo.save(blog);
         
        List<BlogPosting> postings = new ArrayList<>();
        postings = blogRepo.findGreterThan5();
        return postings;
    }

    public String saveBlog(RequestPosting req) {
        BlogPosting blog = new BlogPosting();
         blog.setTitle(req.getTitle());
         blog.setContent(req.getContent());
         blogRepo.save(blog);
        return "sukses";
    }
}
