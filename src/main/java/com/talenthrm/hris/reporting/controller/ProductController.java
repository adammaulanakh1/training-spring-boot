package com.talenthrm.hris.reporting.controller;

import java.util.List;

import com.talenthrm.hris.reporting.dto.RequestProducts;
import com.talenthrm.hris.reporting.model.BlogProducts;
import com.talenthrm.hris.reporting.model.Products;
import com.talenthrm.hris.reporting.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/log")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<?> fungsi(){
        List<BlogProducts> posts = productService.listBlogs();
        return ResponseEntity.ok(posts);
    }
    @PostMapping("/products")
    public ResponseEntity<?> fungsiasd(@RequestBody RequestProducts req){
        String hasil = productService.saveBlog(req);

        return ResponseEntity.ok(hasil);
    }
    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable Long id){
        return productService.deleteProduct(id);
    }
    @PutMapping("/update")
    public BlogProducts updateProducts(@RequestBody RequestProducts blogproducts){
        return productService.updateProducts(blogproducts);
    }
}
