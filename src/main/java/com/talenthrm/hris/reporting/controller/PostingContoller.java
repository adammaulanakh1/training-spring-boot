package com.talenthrm.hris.reporting.controller;

import java.util.ArrayList;
import java.util.List;

import com.talenthrm.hris.reporting.dto.RequestPosting;
import com.talenthrm.hris.reporting.model.BlogPosting;
import com.talenthrm.hris.reporting.repository.BlogRepository;
import com.talenthrm.hris.reporting.service.PostingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/blog")
public class PostingContoller {
    @Autowired
    PostingService postingService;

    @GetMapping("/posting")
    public ResponseEntity<?> fungsi() {
        List<BlogPosting> posts = postingService.listBlogs();
        return ResponseEntity.ok(posts);
    }
    @PostMapping("/posting")
    public ResponseEntity<?> fungsiasd(@RequestBody RequestPosting req) {

       String hasil = postingService.saveBlog(req);

        return ResponseEntity.ok(hasil);
    }
}