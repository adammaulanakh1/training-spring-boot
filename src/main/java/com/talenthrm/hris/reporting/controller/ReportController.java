package com.talenthrm.hris.reporting.controller;

import com.talenthrm.hris.reporting.dto.RequestLogin;
import com.talenthrm.hris.reporting.dto.ResponseLogin;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ReportController {

    @GetMapping("/report")
    public ResponseEntity<?> fungsi() {
        String response = "ini resposne /report";
        return ResponseEntity.ok(response);
    }

    @PostMapping("/login")
    public ResponseEntity<?> yanglain(@RequestBody RequestLogin reqData) {
        ResponseLogin res = new ResponseLogin();
        if (reqData.getPassword().equals("rahasia")) {
            res.setMessage("anda sudah login");
            res.setStatus("logged in");
            return ResponseEntity.ok(res);
        } else {
            res.setMessage("password anda salah");
            res.setStatus("not logged in");
            return ResponseEntity.ok(res);

        }
    }

}
