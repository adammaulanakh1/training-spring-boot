package com.talenthrm.hris.reporting.dto;

import lombok.Data;
@Data
public class RequestLogin {
    String username;
    String password; 
}
