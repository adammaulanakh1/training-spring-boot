package com.talenthrm.hris.reporting.dto;

import lombok.Data;

@Data
public class ResponseLogin {
    String status;
    String message;
}
