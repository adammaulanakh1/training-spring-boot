package com.talenthrm.hris.reporting.dto;

import lombok.Data;

@Data
public class RequestPosting {
    String title;
    String content;
}

