package com.talenthrm.hris.reporting.dto;

import lombok.Data;

@Data
public class RequestProducts {
    String amount;
    String color;
    String price;
    String rating;
}
