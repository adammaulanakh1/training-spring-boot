package com.talenthrm.hris.reporting.repository;

import java.util.List;

import com.talenthrm.hris.reporting.model.BlogProducts;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<BlogProducts, Long>{
    @Query("select blog from BlogProducts blog where blog.id < 9 ")
    List<BlogProducts> findGreterThan5();

    void deleteById(int id);

    
}
