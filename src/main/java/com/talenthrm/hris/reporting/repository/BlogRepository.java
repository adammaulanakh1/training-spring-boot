package com.talenthrm.hris.reporting.repository;

import java.util.List;

import com.talenthrm.hris.reporting.model.BlogPosting;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BlogRepository extends JpaRepository<BlogPosting, Long> {
    @Query("select blog from BlogPosting blog where blog.id < 10 ")
    List<BlogPosting> findGreterThan5();

    
}
